package uk.gov.dwp.discintersections;

import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

public class TestAcceptanceCases {
    private Solution testSolution = new Solution();
    @Test
    public void acceptanceTestSixElementsElevenIntersects() {
        Assert.assertThat(testSolution.calculateIntersections(new int[] {1,5,2,1,4,0}), is(equalTo(11)));
    }
    @Test
    public void acceptanceTestLargeIntegers() {
        Assert.assertThat(testSolution.calculateIntersections(new int[] {2_147_483_647,2_147_483_647}), is(equalTo(1)));
    }
}
