package uk.gov.dwp.discintersections;

import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

public class TestSolution {
    private Solution testSolution = new Solution();
    @Test
    public void givenASingleElementArrayTheResultIsAlwaysZero() {
        Assert.assertThat("Expected output from [n] is 0",
                testSolution.calculateIntersections(new int [] {1}), is(equalTo(0)));
    }
    @Test
    public void itReturnsOneForTwoIntersectingDiscs() {
        Assert.assertThat("Expected output for two intersecting discs is 1",
                testSolution.calculateIntersections(new int [] {2,2}), is(equalTo(1)));
    }
    @Test
    public void itReturnsZeroForTwoPoints() {
        Assert.assertThat("Expected output for two points is 0",
                testSolution.calculateIntersections(new int [] {0,0}), is(equalTo(0)));
    }
    @Test
    public void itReturnsOneForTwoTouchingDiscs() {
        Assert.assertThat("Expected output for two touching discs is 1",
                testSolution.calculateIntersections(new int [] {0,1}), is(equalTo(1)));
    }
    @Test
    public void itReturnsOneWhereOneDiscIsContainedInAnother() {
        Assert.assertThat("Expected output for one disc containing another is 1",
                testSolution.calculateIntersections(new int [] {1,3}), is(equalTo(1)));
    }
    @Test
    public void itReturnsMinusOneIfTheNumberOfPairsExceedsTenMillion() {
        int[] sample = new int[10000];
        for (int i=0;i<10000;++i) {
            sample[i] = 10000;
        }
        Assert.assertThat("Expected output when intersections exceeds 10_000_000 is -1",
                testSolution.calculateIntersections(sample),is(equalTo(-1)));
    }
    @Test
    public void IntersectAcceptsTwoDiscsAndReturnsTrueIfTheyIntersect() {
        Assert.assertThat("Expected true for {1,2} and {3,2}",
                testSolution.intersect(1,2,3,2), is(true));
    }
    @Test
    public void IntersectAcceptsTwoDiscsAndReturnsTrueIfTheyTouch() {
        Assert.assertThat("Expected true for {1,1} and {3,1}",
                testSolution.intersect(1,1,3,1), is(true));
    }
    @Test
    public void IntersectAcceptsTwoDiscsAndReturnsTrueIfOneContainsTheOther() {
        Assert.assertThat("Expected true for {1,1} and {3,5}",
                testSolution.intersect(1,1,3,5), is(true));
    }
    @Test
    public void IntersectAcceptsTwoDiscsAndFalseTrueIfTheyDontTouch() {
        Assert.assertThat("Expected true for {1,1} and {4,1}",
                testSolution.intersect(1,1,4,1), is(false));
    }
    @Test
    public void IntersectAcceptsTheDiscsInAnyOrder() {
        Assert.assertThat("Expected true for {3,2} and {1,2}",
                testSolution.intersect(3,2,1,2), is(true));
    }
    @Test
    public void ForwardIntersectionsAcceptsAnArrayAndAnIndexAndReturnsTheNumberOfIntersectsForTheIndexForwards() {
        Assert.assertThat("Expected 0 for 0 and [0,0,1]",
                testSolution.forwardIntersections(0, new int[] {0,0,1}), is(equalTo(0)));
        Assert.assertThat("Expected 1 for {0} and [0,1,1]",
                testSolution.forwardIntersections(0, new int[] {0,1,1}), is(equalTo(1)));
        Assert.assertThat("Expected 2 for {0} and [1,0,1]",
                testSolution.forwardIntersections(0, new int[] {1,0,1}), is(equalTo(2)));
        Assert.assertThat("Expected 1 for {1} and [1,0,1]",
                testSolution.forwardIntersections(1, new int[] {1,0,1}), is(equalTo(1)));
    }
}
