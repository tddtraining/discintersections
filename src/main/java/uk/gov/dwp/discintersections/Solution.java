package uk.gov.dwp.discintersections;

public class Solution {
    private int MAXIMUM_INTERSECTIONS_LIMIT = 10_000_000;

    public int calculateIntersections(int [] A) {
        int result = 0;
        for (int index = 0; index < A.length - 1; ++index) {
            result += forwardIntersections(index, A);
            if (result > MAXIMUM_INTERSECTIONS_LIMIT) {
                result = -1;
                break;
            }
        }
        return result;
    }

    boolean intersect(int indexA, int radiusA, int indexB, int radiusB) {
        boolean result;
        if (indexA > indexB) {
            result = intersect(indexB, radiusB, indexA, radiusA);
        } else {
            int rightBoundary = indexA + radiusA;
            int leftBoundary = indexB - radiusB;
            result = leftBoundary <= rightBoundary;
        }
        return result;
    }

    int forwardIntersections(int index, int[] A) {
        int result = 0;
        for (int innerIndex = index + 1; innerIndex < A.length; ++innerIndex) {
            if (intersect(index, A[index], innerIndex, A[innerIndex])) {
                ++result;
            }
        }
        return result;
    }
}
